var extend = require('underscore/cjs/extend.js');
var reduce = require('underscore/cjs/reduce.js');
function add(a, b) { return a + b; }
var extended = extend({a: 1, b: 2}, {b: 3, c: 4});
module.exports = reduce(extended, add, 0);
