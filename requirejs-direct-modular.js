var requirejs = require('requirejs');

requirejs.config({
    paths: {
        'underscore/modules': 'node_modules/underscore/amd',
    },
});

requirejs(['modular-amd'], function(answer) {
    console.log(answer);
});
