#!/usr/bin/env sh

yarn add rollup@1 @rollup/plugin-node-resolve@7 webpack-cli@3 --no-lockfile && ./ci-script-common.sh && yarn build-webpack-modular-node8 && yarn run-webpack && yarn build-webpack-monolithic-node8 && yarn run-webpack && yarn test-node10-direct-esm
