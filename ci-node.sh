#!/usr/bin/env sh

./ci-script-common.sh && ./ci-webpack-common.sh && yarn test-node-direct-esm && yarn test-node-direct-esm-named && yarn test-node-direct-esm-modular
