#!/usr/bin/env sh

export SIDELOAD=./underscore-sideload/

test -d $SIDELOAD && yarn add $SIDELOAD || yarn
