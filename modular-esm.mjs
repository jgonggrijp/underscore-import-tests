import extend from 'underscore/modules/extend.js';
import reduce from 'underscore/modules/reduce.js';
function add(a, b) { return a + b; }
var extended = extend({a: 1, b: 2}, {b: 3, c: 4});
export default reduce(extended, add, 0);
