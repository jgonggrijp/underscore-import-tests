define([
    'underscore/modules/extend',
    'underscore/modules/reduce',
], function(extend, reduce) {
    function add(a, b) { return a + b; }
    var extended = extend({a: 1, b: 2}, {b: 3, c: 4});
    return reduce(extended, add, 0);
});
