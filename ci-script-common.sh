#!/usr/bin/env sh

./ci-install-common.sh && yarn test-node-direct-cjs && yarn test-node-direct-cjs-modular && yarn test-browserify-modular && yarn test-browserify-monolithic && yarn test-rjs-direct-modular && yarn test-rjs-direct-monolithic && yarn test-rjs-optimized-modular && yarn test-rjs-optimized-monolithic && yarn test-rollup-modular && yarn test-rollup-monolithic
