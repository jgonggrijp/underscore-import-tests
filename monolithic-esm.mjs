import _ from 'underscore';
function add(a, b) { return a + b; }
var assigned = _.assign({a: 1, b: 2}, {b: 3, c: 4});
export default _.reduce(assigned, add, 0);
