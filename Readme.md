# Underscore import test suite

This is a set of tests that attempts to assess whether a particular version of [Underscore](https://underscorejs.org/) (as a package) is still importable through various tools such as Node.js, Require.js and Rollup. The tests run inside Docker containers in order to make it a bit easier to run them in multiple versions of Node.js.


## How to run the tests

You need to have `git`, `npm` and `docker-compose` installed on your system.

```sh
git clone https://gitlab.com/jgonggrijp/underscore-import-tests.git
cd underscore-import-tests
npm run test
```

You can also do `yarn test` instead of `npm run test`, but that will install the dependencies as a side effect, which is unnecessary because they are installed *inside the docker container* during testing. So `npm run test` saves you fifty megabytes of disk space.

**Do not run docker-compose up.** That will (try to) run the tests on all Node.js versions *in parallel*, which will cause race conditions on the shared resources. The `test` script takes care of running the Node.js versions *sequentially* and also cleans up the side effects on the `package.json` and the `yarn.lock` afterwards.


## Sideloading a local development Underscore

If you have an unpublished local development version of Underscore, you can sideload it into the test suite by copying the build output to an `underscore-sideload` directory next to this Readme. Include the `package.json` so `yarn` will recognize it as a package. Exclude the `node_modules`, `coverage`, `docs` and `.git` subdirectories in order to speed up testing.


## Hacking and debugging

The following command will let you interact with the Node.js v14 docker container in order to try things out, update the `yarn.lock` etcetera. Of course, you can replace `14` by another version number.

```sh
docker-compose run test-14 sh
```

Be sure to stash or commit your changes to the `package.json` and the `yarn.lock` before running the tests again.


## Cleaning up after testing

```sh
npm run clean  // (or yarn clean)
```
