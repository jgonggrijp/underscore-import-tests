import { assign, reduce } from 'underscore';
function add(a, b) { return a + b; }
var assigned = assign({a: 1, b: 2}, {b: 3, c: 4});
export default reduce(assigned, add, 0);
